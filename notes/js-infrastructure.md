# Containerisation with Docker
## Linux containers
Why containers?
- control groups: limit resources a process can use (for example memory or CPU usage)
- namespaces: to limit what a process can see

-> great features but low-level to configure
-> say hi to container systems: abstractions to get around the control groups and namespace.

## Docker containers
The container breakthrough happened because of Docker. Containers are built from Dockerfiles.

To build a Docker container (once you've created the Dockerfile):

`docker build -t name-of-container .`

. -> is for the location op the Dockerfile (so in our case it is the current directory)

## Connecting the containers
We have our app in a container and we run MongoDB in another container -> they now need to be connected with each other.

To do this, they need to be on the same Docker network. Create it with:

`docker network create name-of-nework`

And then run MongoDb with that

```
docker run --name=mongo --rm --network=name-of-network mongo
```

--rm automatically cleans up the container and removes the file system when the container exits

-> MongoDB is running

To run the app itself:

```
docker run --name=name-for-container --rm --network=name-of-network -p 3000:3000 -e MONGO_URL=mongodb://mongo:27017/dev name-of-container
```

-p 3000:3000 publishes port 3000 of the container to port 3000 of your local machine. That means, if you now access port 3000 on your computer, the request is forwarded to port 3000 of the container. You can use the forwarding to access the app from your local machine.

-e sets an environment variable inside the container

in the MONGO_URL, it says `mongo:27017` -> this `mongo` value comes from what we used as the `--name` value when we started the Mongo container. Containers in the same network can talk to each other by their name iso IP address, thanks to a build-in DNS mechanism.

To see all the containers that are running, use `docker ps`

To stop and remove the apps:
```
docker stop mongo name-of-container
```

## Publishing a Docker image
Uploaded images need to have their name in the following format: username/imagename:tagname

# Deploying to Kubernetes
When running containers in production, you should be able to scale every microservice independently. To do this you need to use a container orchestator. They are designed to run complex applications with large numbers of scalable components.

Examples of container orchestrators are Hashicorp, Nomad and Kubernetes,

## Creating a local k8s cluster
We will use Minikube to do this. Minikube creates a single-node Kubernetes cluster running in a virtual machine. This should only be used for testing purposes.

With `minikube start` you will create a virtual machine and install k8s. Once done, to verify it was created used `kubectl cluster-info`.

## k8s resources
it has a "declarative interface" -> you describe how you want the deployment to look lik eand k8s figures out the steps to do this. To communicate with k8s you use k8s resources. These are definied in YAML files and submitted to the cluster with the API.

-> you can do all these with kubectl

### setup the deployment
Create a folder. The purpose of this folder is to hold all the Kubernetes YAML files that you will create.

It's a best practice to group all resource definitions for an application in the same folder because this allows to submit them to the cluster with a single command.

A Deployment creates and runs containers and keeps them alive.

#### Pods
A pod is a wrapper around one or more containers. Usually a pod contains a single container but for advanced cases it can contain multiple containers.

Technically, a Pod is a Kubernetes resource, like a Deployment or Service.

### Setup a service
Once you've created the deployment resource, you have defined how to run the app in the cluster. Now you still need to make it available to other pods in the cluster or users outside of it. To do this, you need to define a service resource.

It is best practice to define resource definition for the same application in the same YAML file.

Pro tip: find out about all available Service types with `kubectl explain service.spec.type`

Beyond exposing your containers, a Service also ensures continuous availability for your app.

### Defining the database tier
The trick here is that you want the storage to persist when the pod is deleted or moved to another node.

You need to add another resource type `PersistentVolumeClaim`. So to setup the database component, you need the following resource definitions:

- PersistentVolumeClaim
- Service
- Deployment

### Deploying the application
First check if your Minikube cluster is running

`minikube status`

Then submit the resource definitions. The following command will submit all the YAML files in the `kube` directory.

`kubectl apply -f kube`

To keep an eye on the pods "coming to live", you can use this

`kubectl get pods --watch`

Once they are in running state, you can access your service with minikube with `minikube service knote`

To remove the cluster, run `kubectl delete -f kube`

# Scaling
To be scalable, applications must be stateless. Stateless means that an instance can be killed restarted or duplicated at any time without any data loss or inconsistent behaviour.

```
kubectl scale --replicas=10 deployment/knote
```

# K8s in the cloud
